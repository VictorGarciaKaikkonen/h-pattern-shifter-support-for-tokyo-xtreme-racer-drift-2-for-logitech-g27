# H pattern shifter support for Tokyo Xtreme Racer: Drift 2 (NTSC) - Kaido Racer 2 (PAL) for Logitech G27



## What's this?
A Python script that changes memory values from PCSX2 to add H pattern shifter support on the Logitech G27 wheel.

## How does it work?
The script works by modifying certain memory values to trick the game into thinking the car is in a different gear than it actually is. Let's say you are in 5th and want to go straight into 2nd. When you select 2nd in the shifter, the "gear" memory value becomes "3" and then the script automatically triggers the downshift key.

## What doesn't work

It has been tested on the European version of the game, but it should work the same way in the US release.

The clutch doesn't work and right now I don't know how I could make it work. Just do the clutch work and don't think too much about it, forget about clutch kicking, though.

Reverse gear doesn't work.

## Installation

Make sure your wheel works as intended in PCSX2 with the [USBqemu-wheel](https://forums.pcsx2.net/Thread-Qemu-USB-plugin-formerly-known-as-USBqemu-wheel) plugin first.

Change the initial constant section of `wheel.py` to suit your needs.

Just run `wheel.py` once you've connected your wheel and run the game. Ignore any errors, those are normal, the script should actually work.

## TODO:

- Implement support for reverse gear.
- Wait for the wheel to be connected.
- Add support for multiple joysticks connected at the same time. You may need to change the value `0` in `joystick = pygame.joystick.Joystick(0)` to some other number if you have multiple devices such as wheels, handbrakes, etc. connected to your PC.
- Add clutch support (I don't know if this is even possible).
