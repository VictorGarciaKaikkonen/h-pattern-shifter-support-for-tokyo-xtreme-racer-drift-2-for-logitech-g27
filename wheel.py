from os import getpid
import keyboard
from ctypes import *
from ctypes.wintypes import *
import psutil
import time
from pymem.process import set_debug_privilege
import win32api
from keyboardctypes import PressKey, ReleaseKey
import pymem
import pygame.joystick
import sys

# ------------------------------------------------------------------------------
# Change the variables in this section according to your needs

# Uncomment the one corresponding to your region
ADDRESSES = [0x20B9FE34, 0x20B9245C] # Addresses for the European (PAL) version
# ADDRESSES = [0x20B84E0C, 0x20B927E4] # Addresses for the US (NTSC) version

# Key codes available at: https://docs.microsoft.com/en-us/windows/win32/inputdev/virtual-key-codes
UPSHIFT_KEY_HEX_CODE = 0x58 # Hexadecimal code for the upshift key
DOWNSHIFT_KEY_HEX_CODE = 0x5A # Hexadecimal code for the downshift key
# ------------------------------------------------------------------------------



KEY_DOWN_TIME = 0.05 # Time the key will be held down

pid = None

pm = None
def initProcessPymem():
    global pm
    global pid
    print("Waiting for pcsx2.exe to be present in memory")
    pid = findProcessIdByName("pcsx2")
    pm = pymem.Pymem()
    pm.open_process_from_id(pid)
    set_debug_privilege(pm.process_handle, 'SeDebugPrivilege', True)
    print("pcsx2.exe pid: "+str(pid))
    print("Init done")

def findProcessIdByName(processName, wait=True):
    '''
    Get a list of all the PIDs of a all the running process whose name contains
    the given string processName.
    It will wait if the process isn't present
    '''

    listOfProcessObjects = []
    res = []
    #Iterate over the all the running process
    
    present = False
    while not present:
        for proc in psutil.process_iter():
            try:
                pinfo = proc.as_dict(attrs=['pid', 'name', 'create_time'])
                # Check if process name contains the given name string.
                if processName.lower() in pinfo['name'].lower():
                    listOfProcessObjects.append(pinfo)
                present = True
            except (psutil.NoSuchProcess, psutil.AccessDenied , psutil.ZombieProcess):
                if wait:
                    present = False
                    time.sleep(1)
                else:
                    present = True
                    res[0] = -1
            
            for elem in listOfProcessObjects:
                res.append(elem['pid'])

    return int(res[0])

def writeToMemoryPymem(value, addresses):
    global pm
    value = int(value)
    for address in addresses:
        pm.write_int(address, value)


def wheelRoutine():
    currentGear = 0
    gearRequest = 0
    while True:
        gearRequest = getWheelGear()
        if gearRequest != 0 and gearRequest != currentGear:
            if gearRequest < currentGear: # Downshift
                writeToMemoryPymem(gearRequest+1, ADDRESSES)
                PressKey(DOWNSHIFT_KEY_HEX_CODE)
                time.sleep(KEY_DOWN_TIME)
                ReleaseKey(DOWNSHIFT_KEY_HEX_CODE)
            else: # Upshift
                writeToMemoryPymem(gearRequest-1, ADDRESSES)
                PressKey(UPSHIFT_KEY_HEX_CODE)
                time.sleep(KEY_DOWN_TIME)
                ReleaseKey(UPSHIFT_KEY_HEX_CODE)
                
            currentGear=gearRequest
            
        time.sleep(0.001)

            
        

joystick = None
def initWheel():
    global joystick
    pygame.init()
    pygame.joystick.init()
    joystick = pygame.joystick.Joystick(0)
    joystick.init()
    print(joystick.get_name())
    print(joystick.get_numaxes())

def showWheelInputs(): #Debugging function
    global joystick
    while True:
        pygame.event.pump() 
        print("First: " + str(joystick.get_button(8))) #First gear
        print("Second: " + str(joystick.get_button(9))) #Second gear
        print("Third: " + str(joystick.get_button(10))) #Third gear
        print("Fourth: " + str(joystick.get_button(11))) #Fourth gear
        print("Fifth: " + str(joystick.get_button(12))) #Fifth gear
        print("Sixth: " + str(joystick.get_button(13))) #Sixth gear
        print("Reverse: " + str(joystick.get_button(14))) #Reverse gear
        print("\n")
        time.sleep(0.5)

def getWheelGear():
    pygame.event.pump() # If we don't do this, we won't get an input reading from the wheel
    for i in range(8,14):
        if joystick.get_button(i):
            return int(i - 7) # Mapping the button number to the actual gear

    if joystick.get_button(14) == 1: # Reverse, which doesn't actually work
        return int(-1)
    else:
        return int(0)

#TODO: wait for wheel

initProcessPymem()
initWheel()
wheelRoutine()
#showWheelInputs()